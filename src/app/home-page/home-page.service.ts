import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './product';

@Injectable({
  providedIn: 'root',
})
export class HomePageService {
  private products: Product[] = [
    {
      id: 1,
      title: 'Tandoori Pork Roll',
      description:
        'This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product1.jpeg',
    },
    {
      id: 2,
      title: 'Masala Chicken Pack',
      description:
        'This is the description for Masala Chicken Pack. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product2.jpeg',
    },
    {
      id: 3,
      title: 'Masala Prawns Roll',
      description:
        'This is the description for Masala Prawns Roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product3.jpeg',
    },
    {
      id: 4,
      title: 'Kashmiri Pork Combo',
      description:
        'This is the description for Kashmiri Pork Combo. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product4.jpeg',
    },
    {
      id: 5,
      title: 'Masala Mushroom Salad',
      description:
        'This is the description for Masala Mushroom Salad. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product5.jpeg',
    },
    {
      id: 6,
      title: 'Punjabi Fish Combo',
      description:
        'This is the description for Punjabi Fish Combo. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product6.jpeg',
    },
    {
      id: 7,
      title: 'Masala Prawns Salad',
      description:
        'This is the description for Masala Prawns Salad. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product7.jpeg',
    },
    {
      id: 8,
      title: 'Tandoori Mushroom Roll',
      description:
        'This is the description for Tandoori Mushroom Roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product8.jpeg',
    },
    {
      id: 9,
      title: 'Thai Mushroom Roll',
      description:
        'This is the description for Thai Mushroom Roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product9.jpeg',
    },
    {
      id: 10,
      title: 'Thai Rice Salad',
      description:
        'This is the description for Thai Rice Salad. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product10.jpeg',
    },
    {
      id: 11,
      title: 'Thai Pork Combo',
      description:
        'This is the description for Thai Pork Combo. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product11.jpeg',
    },
    {
      id: 12,
      title: 'Jain Egg Combo',
      description:
        'This is the description for Jain Egg Combo. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product12.jpeg',
    },
    {
      id: 13,
      title: 'Italian Dal Salad',
      description:
        'This is the description for Itlaian Dal Salad. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product13.jpeg',
    },
    {
      id: 14,
      title: 'South Indian Paneer Plate',
      description:
        'This is the description for South Indian Paneer Plate. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product14.jpeg',
    },
    {
      id: 15,
      title: 'Punjabi Nuts Roll',
      description:
        'This is the description for Punjabi Nuts Roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product15.jpeg',
    },
    {
      id: 16,
      title: 'Malasian Fish Combo',
      description:
        'This is the description for Malasian Fish Combo. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product16.jpeg',
    },
    {
      id: 17,
      title: 'Masala Mutton Meal',
      description:
        'This is the description for Masala Mutton Meal. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product17.jpeg',
    },
    {
      id: 18,
      title: 'Malasian Pork Roll',
      description:
        'This is the description for Malasian Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product18.jpeg',
    },
    {
      id: 19,
      title: 'Punjabi Mutton Meal',
      description:
        'This is the description for Punjabi Mutton Meal. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product19.jpeg',
    },
    {
      id: 20,
      title: 'Malasian Egg Plate',
      description:
        'This is the description for Malasian Egg Plate. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq.This is the description for Tandoori Pork roll. Kh Fyf Xuxwcde Qjhsb Hg Alwv Nt Fudg Dgfttq',
      imageUrl: '../../assets/images/product20.jpeg',
    },
  ];

  private viewerIds: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  constructor(private httpClient: HttpClient) {}

  getProducts(): Product[] {
    return this.products;
  }

  getViewerIds(): number[] {
    return this.viewerIds;
  }

  findProduct(id: number): Product {
    const result: Product = this.products.find((product) => {
      return product.id === id;
    });
    return result;
  }
}
