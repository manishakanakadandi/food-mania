import { Component, OnInit } from '@angular/core';
import { HomePageService } from './home-page.service';
import { Product } from './product';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {
  products: Product[];
  viewerIds: number[];
  currentViewerId: number = 1;

  constructor(private homePageService: HomePageService) {}

  ngOnInit(): void {
    this.products = this.homePageService.getProducts();
    this.viewerIds = this.homePageService.getViewerIds();
  }

  setViewerId(viewerId: number): void {
    this.currentViewerId = viewerId;
  }

  setClasses(viewerId: number) {
    let classes = {
      'btn btn-primary': true,
      'btn-selected': this.currentViewerId === viewerId,
    };
    return classes;
  }
}
