import { HomePageComponent } from './home-page/home-page.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DetailsPageComponent } from './details-page/details-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
const appRoutes = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'product/:product_id/reviews/:viewer_id',
    component: DetailsPageComponent,
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
