import { Component, OnInit } from '@angular/core';
import { DetailspageService } from '../details-page/detailspage.service';
import { ActivatedRoute } from '@angular/router';
import { Review } from '../details-page/product-review';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
})
export class PaginationComponent implements OnInit {
  reviews: Review[];
  pageNumber: number = 1;
  size: number = 3;

  constructor(
    private detailsPageService: DetailspageService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const productId = this.route.snapshot.params['product_id'];
    const viewerId = this.route.snapshot.params['viewer_id'];
    this.detailsPageService
      .getReviews(productId, viewerId)
      .subscribe((productReview) => {
        this.reviews = productReview.reviews;
      });
  }

  showMoreOrLess(review: Review): void {
    review.canShowMore = !review.canShowMore;
  }

  getShowMoreOrLess(review: Review): string {
    if (review.canShowMore) return 'Show Less';
    return 'Show More';
  }

  getReviewerName(review: Review): string {
    if (review.friend) return review.reviewer.name + ' Says';
    return 'A Customer Says';
  }
}
