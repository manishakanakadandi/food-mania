interface Reviewer {
  id: number;
  name: string;
  sex: string;
  age: number;
  biography: string;
  email: string;
}

export interface Review {
  comment: string;
  friend: boolean;
  friend_type: Object;
  like: boolean;
  product_id: number;
  ratings: Object;
  reviewer: Reviewer;
  self_review: boolean;
  title: string;
  usefulness: number;
  user_id: number;
  viewer_useful: boolean;
  canShowMore: boolean;
  showMoreOrLess: string;
}

export interface ProductReview {
  product_id: number;
  reviews: Array<Review>;
  viewer_id: number;
}
