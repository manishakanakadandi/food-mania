import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HomePageService } from '../home-page/home-page.service';
import { Product } from '../home-page/product';
import { Review } from './product-review';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.css'],
})
export class DetailsPageComponent implements OnInit {
  product: Product;

  constructor() {}

  ngOnInit(): void {
    this.product = history.state.data;
  }
}
