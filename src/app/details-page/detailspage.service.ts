import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductReview } from './product-review';

@Injectable({
  providedIn: 'root',
})
export class DetailspageService {
  constructor(private httpClient: HttpClient) {}

  getReviews(productId: number, viewerId: number): Observable<ProductReview> {
    return this.httpClient.get<ProductReview>(
      `api/reviews/${productId}/${viewerId}`
    );
  }
}
